object B_CompanionObjects {

  // Companion objects are where static properties and functions live
  // Remember I said Scala takes OOP very seriously? This is a good example of what I meant. Scala does not have any
  // concept of 'statics'. Everything is attached to a class or object.
  // All objects are actually Singletons (implemented in Scala, so there's no need to implement it yourself)
  object Box {
    def square(size: Int): Box = new Box(size, size, size)

    // apply is a special function that can be called without it's name
    def apply(width: Int, height: Int, depth: Int): Box = new Box(width, height, depth)
  }
  class Box(val width: Int, val height: Int, val depth: Int) {
    val volume: Int = width * height * depth
    def biggerThan(that: Box): Boolean = this.volume > that.volume
  }

  val square = Box.square(10)
  val myBox = Box(10, 11, 12) // this is actually Box.apply(10, 11, 12)

  // Operators
  square.biggerThan(myBox)
  // or
  square biggerThan myBox // the method only takes a single arg so you can drop the . and the ()
                          // this is what makes Scala such a flexible language, simple syntax rules that make it
                          // possible to "extend" the language
}
