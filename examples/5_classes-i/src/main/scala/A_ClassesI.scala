object A_ClassesI {
  // is this class imperative or functional??
  class Box1(w: Int, h: Int, d: Int) { // this is the constructor for Box
    // The body of the class is the constructor
    var width: Int = w
    var height: Int = h
    var depth: Int = d  // properties and functions are public by default

    def volume: Int = width * height * depth
  }
  var b1 = new Box1(10, 3, 5)


  // adding val or var to the constructor turns those arguments into properties
  class Box2(var width: Int, var height: Int, var depth: Int) {
    def volume: Int = width * height * depth
  }
  var b2 = new Box2(10, 3, 4)


  // functional version
  class Box3(val width: Int, val height: Int, val depth: Int) {
    val volume: Int = width * height * depth  // notice how this has changed to a val because Box3 is immutable!
                                              // Also notice how similar the syntax is val vs def, pure, zero arity defs
                                              // are actually very much like variables!
  }
  val b3 = new Box3(10, 4, 2)


  // alternate constructors
  class Box4(val width: Int, val height: Int, val depth: Int) {
    // alternate constructors must be defined in terms of other constructors ultimately ending in a call to the main
    // constructor. They work like a funnel
    def this(size: Int) {
      this(size, size, size)
    }

    val volume: Int = width * height * depth
  }
  val b4 = new Box4(10)

  // ** SHOW THE TYPE HIERARCHY SLIDE
  // Lets take a brief moment the look at the basic type hierarchy in Scala
}
