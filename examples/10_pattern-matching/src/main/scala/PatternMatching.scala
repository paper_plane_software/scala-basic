import scala.util.Random

object PatternMatching {
  // Pattern matching are like if statements on steroids, we'll start with a very simple example

  // SYNTAX
  val i = Random.nextInt(2)
  i match {
    case 0 => println("true")   // the statement after 'case' and before '=>' is the pattern that we're matching against
    case 1 => println("false")
    case _ => println("wat!")   // here is the underscore syntax again. And just like in other places it means "I
                                // don't care", in this case "any value!"
  }

  // it works on other types too
  val name = "oliver"
  name match {
    case "oliver" => println("it's me!")
    case "tom" => println("it's tom!")
    case _ => println("who are you?")
  }

  // They look a lot like Java's switch statement, but they are much more powerful!
  // Firstly, in true functional programming style they are expressions NOT statements
  var age = name match {
    case "oliver" => 35
    case "tom" => 34
    case _ => 0
  }

  // Secondly they do not have to be exhaustive!
  // Q. What do you think would happen if there isn't a match?
  // A. A MatchError exception is thrown
  age = name match {
    case "oliver" => 35
    case "tom" => 34
  }

  // MULTIPLE MATCHES
  val j = Random.nextInt(10)
  j match {
    case 1 | 3 | 5 | 7 | 9 => println("odd")
    case 0 | 2 | 4 | 6 | 8 => println("even")
  }

  // GUARDS
  // Guards allow you to add an if statement to a pattern
  j match {
    case even if even % 2 == 0 => println("even")
    case odd if odd % 2 == 0 => println("odd")
  }

  // CASE CLASSES
  // This is where pattern matching starts to get interesting!
  case class Person(name: String, sex: String, age: Int)
  case class Robot(name: String, killiness: Int, age: Long)

  val oli = Person("oliver winks", "male", 35)

  // TYPE MATCHING
  oli match {
    case _: Person => println("phew I'm a real boy!")
    case _: Robot => println("T1000")                 // this is actually a really stupid test because if your're
                                                      // testing for Person'hood the thing your testing couldn't
                                                      // possibly be a Robot, but this is just to show you the principle
                                                      // later We will look at ADT's and then it becomes useful
  }

  // DESTRUCTURING
  // because case classes implement the `unapply` function we can "extract" or "deconstruct" them using pattern matching
  oli match {
    case Person(name, sex, age) => println(s"my name is $name, I'm a $age year old $sex")
  }
  // in this example we've extracted the properties of the person into separate variables

  // if we're not interested in one or more of the properties we can use the `_` syntax
  oli match {
    case Person(_, _, age) => println(s"my age is $age")
    case Person("oli", _, age) => println(s"match only people named 'oli'")
  }

  // Guards still work
  oli match {
    case Person(_, _, age) if age > 70 => println("old timer")
    case Person(_, _, age) if age > 40 => println("middle aged")
    case _ => println("young'n")
  }

  // Complex patterns
  case class Family(father: Person, mother: Person, child: Person)
  val family = Family(Person("Oli", "M", 35), Person("Sarah", "F", 34), Person("Astrid", "F", 0))
  family match {
    case Family(_, _, Person(_, "F", age)) => println(s"I have a daughter and she's $age")
  }

  // LISTS
  // It's not just case classes that implement extractors, other classes do as well
  // Remember that cons notation we looked at before, that syntax can also be used in pattern matching
  val xs = List(1,2,3,4,5)
  xs match {
    case Nil => println(s"an empty list")
    case head :: tail => println(s"$head is the first element in the list, $tail is everything else")
    case first :: second :: tail => println(s"$first is the first element, $second is the second element, $tail is everything else")
  }

  // Destructuring lists like this (head, tail) is actually a really useful way to implement recursive functions.


  // ** GOTO EXERCISES 3.1 (a couple of pattern matching exercises ending in implementing reduce/fold)
}
