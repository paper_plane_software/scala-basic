object FunctionsII {
  // all operators are actually functions in disguise
  val number = 42
  val numberPlus1 = number + 1
  val numberPlus2 = number.+(2)

  // FUNCTIONS AS VARIABLES (FUNCTION LITERALS)
  // Remember what I said yesterday about functional language treating functions as 1st class citizens? This is a good
  // example of that.
  def add1(i: Int): Int = i + 1
  val add2 = (i: Int) => i + 2 // This is a variable and variables must have a type! What's the type though?

  // Notice the type parameters, these are generics in Scala, we will discuss more about them later but for now just
  // know that they are the same as in Java
  val func1: Function1[Int, Int] = (i: Int) => i + 3                  // Function1 because it takes 1 param
  val func2: Function2[Int, Int, Int] = (x: Int, y: Int) => x*y       // Function2 because it takes 2 params
  val func3: Function2[Int, Int, Double] = (x: Int, y: Int) => x / y  // The last type param is the return type
  val func4: (Int) => Int = (x:Int) => x + 1                          // (Int) => Int shorthand for Function1[Int, Int]
  // You have to draw the line somewhere, Functions can have a maximum of 22 params

  // everything to the right of the = sign is known as a function literal.
  // in exactly the same way that 5 is an integer literal or 3.4 is a double literal

  /* ******** EXERCISE ******** */

  // Now you know you that functions behave just like variables we're going to write some functions that take functions
  // as parameters (these are called higher order functions AKA lambda's)

  // 1. Write a function (give it an appropriate name) that takes a function (you decide the signature of the function)
  // and calls it ten times
  def repeat10Times(func: () => Unit): Unit = for(_ <- 0 until 10) func()

  // 2. Generalise the function so that it repeats `n` times
  def repeatN(n: Int, func: () => Unit): Unit = for(_ <- 0 until n) func()

  // 3. Write a function (give it an appropriate name) that generates a seq of ints by mapping 0 through `n` to ints
  def listFactory(n: Int, func: (Int) => Int): Seq[Int] = for(i <- 0 to n) yield func(i)

  // 4. Call this function passing a function literal

  // 4. Change the function signature so that we can call it with functions defined in the math package (sqrt, cos, sin
  // etc)
  def listFactoryDouble(n: Int, func: (Double) => Double): Seq[Double] = for(i <- 0 to n) yield func(i.toDouble)
  listFactoryDouble(100, math.sqrt)

  /* ******** END EXERCISE ******** */

  // RECAP OF FUNCTION SYNTAX
  // lets remind ourselves of one of the syntax rules we learned yesterday
  // giva a function...
  def repeat10(f: (Int) => Unit): Unit = for(i <- 0 until 10) f(i)

  // we can call this in a few different ways!
  repeat10(i => println(i))  // normal function literal
  repeat10(i => {            // normal function literal (with body)
    println(i)
  })
  repeat10 { i =>  // because `repeat10` only takes a single param we can use {} instead of ()
    println(i)          // in this case we can drop the body {}
  }

  def mySpecialFunction(i: Int): Unit = println(s"Printing $i")
  repeat10(println)           // named function (standard lib)
  repeat10(mySpecialFunction) // named function (custom def)

  // GENERIC FUNCTIONS
  // We've been seeing Types within square brackets coming up a lot.
  // These are called type parameters and they are used to make generic functions and classes
  // Lets take a look at what that means

  /* ******** EXERCISE ******** */

  // 1. Write a function that duplicates an integer value `n` times. The function should return a Seq[Int]
  def duplicate(value: Int, n: Int): Seq[Int] = for(i <- 0 until n) yield value

  // 2. Write a function that duplicates an String value `n` times. The function should return a Seq[String]
  def duplicate(value: String, n: Int): Seq[String] = for(i <- 0 until n) yield value

  // 3. Write a function that duplicates an Boolean value `n` times. The function should return a Seq[Boolean]
  def duplicate(value: Boolean, n: Int): Seq[Boolean] = for(i <- 0 until n) yield value

  // 4. Write a function that .... Please make it stop!
  // See a pattern? The functions are all identical except for the return type and first parameter!

  /* ******** END EXERCISE ******** */

  // Instead of repeating yourself why not make a `generic` function that can work on ANY type?
  def genericDuplicate[T](value: T, n: Int): Seq[T] = for(i <- 0 until n) yield value
}
