public class MessageMaker {
    private String msg;

    public MessageMaker(String msg) {
        this.msg = msg;
    }

    public String getMessage() {
        return this.msg;
    }
}
