object HelloJava {
  def main(args: Array[String]): Unit = {

    // this is a Java class!
    val messageMaker = new MessageMaker("Hello from Java")

    println(messageMaker.getMessage)
  }
}
