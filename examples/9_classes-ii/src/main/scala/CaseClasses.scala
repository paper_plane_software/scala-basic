import java.time.LocalDate

object CaseClasses {
  // Earlier in the course we looked at classes and companion objects
  // case classes are very handy ways of creating classes with companion objects and a bunch of really useful default
  // implementations for some very useful functions.
  case class Person(title: String, name: String, age: Int)
  case class Department(name: String, manager: Person, staff: Seq[Person])
  case class Company(managingDirector: Person, departments: Seq[Department])

  // FUNCTIONAL
  // Case classes are actually normal classes, but by default, any constructor argument is automatically made into
  // a property, even though we didn't specify val or var before them
  val oliver = new Person("mr", "oliver winks", 35)
  oliver.age  // I can access a Person's age

  // case classes are ALWAYS functional, all properties are val's
  // oliver.age = oliver.age + 1  // this is illegal!

  // COMPANION OBJECT
  // Case classes ALWAYS have a companion object with the apply() function implemented
  val tom = Person("mr", "tom parslow", 34)  // notice no 'new' keyword

  // DEFAULT IMPLEMENTATIONS
  tom == oliver // sensible implementation for == (remember == in Scala is value equality NOT reference equality, use eq for that)
  tom.toString  // sensible implementation for toString
  tom.hashCode()  // sensible implementation for hashCode
  tom.copy(age = 35)  // copy is a function that takes the same params as the constructor and creates a new copy of the
                      // object it's called on with the specified property changed

  // EXTRACTOR
  // perhaps most importantly, case classes provide a default implementation for the unapply() function.
  // These functions are sometimes referred to as Extractors and, if we have time, we'll look at what they are.
  // For now it's enough to know that extractors are what allows Pattern Matching to work

  // CASE OBJECTS
  // Just like we have classes and objects in Scala we also have case classes and case objects
  case object Start
  case object Stop

  // case objects cannot take parameters
  // they are often used to represent states or "flags" and are used a lot in ADTs (we'll look at them in a bit)

  // On to Pattern Matching
}
