object Variables {

  var mutable: String = "this is a mutable variable"  // no semi-colon needed, semi-colons are optional
  mutable = "a new string" // Legal

  val immutable: String = "this is an immutable variable"
  // immutable = "a new string" // Illegal

  lazy val lazyVariable: String = "this will be initialized on first use" // Not initialized just yet
  println(lazyVariable) // Now it's initialized!

  // immutable reference to immutable object
  val immutableToImmutable: String = "Strings are immutable, just like Java"
  // immutableToImmutable = "a new string" // This is not allowed because the reference is fixed

  // immutable reference to mutable object
  val immutableToMutable: StringBuilder = StringBuilder.newBuilder
  // immutableToMutable = StringBuilder("a new string")     // Even though StringBuilder is mutable the reference is not
  immutableToMutable.append("we can mutate StringBuilders") // The reference cannot be re-assigned but that doesn't stop
                                                            // us from mutating the object it points to because
                                                            // StringBuilder is a mutable type

  var mutableToImmutable: String = "Strings are immutable"
  mutableToImmutable = "a new String"   // We can't mutate the referenced String, but we can re-assign the reference
                                        // because it's mutable
  // Strings do not contain any functions that return Unit because that would be pointless!

  var mutableToMutable: StringBuilder = StringBuilder.newBuilder  // This is what most Java code is like
  mutableToMutable = StringBuilder.newBuilder                     // We can reassign the reference
  mutableToMutable.append("appended string")                      // Or we can mutate the object pointed to by our
                                                                  // reference

  val typeInference = 42 // The compiler has inferred the type to Int
}
