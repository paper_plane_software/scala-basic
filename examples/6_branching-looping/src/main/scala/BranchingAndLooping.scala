object BranchingAndLooping {
  // If statements work exactly like they do in Java
  var str = ""
  if(10 > 9) {
    str = "bigger than"
  } else {
    str = "less than"
  }

  // except for one thing
  val res = if(10 > 9) "bigger than" else "less than" // they return a value. You'll find that most things in Scala
                                                      // return a value, this is yet another hint at the functional
                                                      // nature of Scala. Notice how the var has been converted to a val
                                                      // having things return values means we can make our programs
                                                      // immutable

  // There are 2 kinds of loops in Scala

  // 1. While loops are the only "statement" in Scala - they don't return a value. They are only useful when writing
  // code in an imperative style. They are not common in Scala code but can be useful in some circumstances
  var i = 0
  while(i < 10) {
    println(s"printing $i") // here is a useful things. Scala has formatted strings
    i += 1
  }

  // 2. For comprehensions (We'll look at why they are called comprehensions rather than loops in a bit!).
  // For comprehensions are similar to for loops in other languages
  for(i <- 0 until 4) {
    println(s"printing $i")
  }

  // Remember how we looked at operators on custom classes - how if a method takes a single argument you can call it
  // without the . or the (). Well 'until' is exactly that
  val range: Range = 0.until(4) // Exclusive

  // Scala takes OOP very seriously. Everything is an Object, even numbers; and they have a function called 'until' and
  // another one called 'to' which return a Range.
  val inclusive = 0 to 4
  val exclusive = 0 until 4

  // The '<-' symbol is an actual built in operator in Scala and it mean 'in' so the line 29 reads:
  // 'for each i in the range 0 until 4 do ...'

  // it's not just Ranges, any collection will do
  for(i <- List(0, 1, 2, 3, 4)) {
    println(s"printing $i")
  }

  // For comprehensions can return a value using the yield keyword
  val double = for(i <- 0 until 10) yield i*2   // This returns a new collection where each element is double the values
                                                // in the Range; IndexedSeq(0, 2, 4, 6, 8, 10, 12, 14, 16, 18)

  // !!WARNING!! Range is an IndexedSeq NOT a List!! This has caught me out in the past
  // Check the type of double!

  // NESTING
  val sum = for(i <- 0 until 10) yield {  // Q. What is the return type?
    for(j <- 0 until 10) yield i + j      // A. Seq[Seq[Int]], annoying right?!
  }

  // Q. What's the return type of this for comprehension?
  val realSum1 = for(i <- 0 until 10; j <- 0 until 10) yield i + j
  val realSum2 = for {
    i <- 0 until 10
    j <- 0 until 10
  } yield i + j // We'll find out why this works later on (this isn't just a handy feature, it's a core part of Scala)

  // FILTERING
  val squares = for(i <- 0 until 100 if math.sqrt(i).isValidInt) yield i

  /* ******** EXERCISE ******** */

  // 1. Representing Triangles as List[Int], create a list of all triangles with sides less than 10
  val exampleTriangle1 = List(1, 5, 6)  // hyp = 1, adj = 5, opp = 6
  val exampleTriangle2 = List(9, 4, 2)  // hyp = 9, adj = 4, opp = 2

  val sides = 1 to 10 // doesn't make sense to have a side length of 0
  val tris = for {
    hyp <- sides
    adj <- sides
    opp <- sides
  } yield List(hyp, adj, opp)


  // 2. Find all the right-angle triangles in that list
  val rightAngles = for {
    hyp <- sides
    adj <- sides
    opp <- sides
    if hyp*hyp == adj*adj + opp*opp   // could import math.pow
  } yield List(hyp, adj, opp)

  // 3. Find all triangles with sides totalling 24
  val twentyFour = for {
    hyp <- sides
    adj <- sides
    opp <- sides
    if hyp*hyp == adj*adj + opp*opp
    if hyp + adj + opp == 24
  } yield List(hyp, adj, opp)

  /* ******** END EXERCISE ******** */

  /* ******** EXERCISE ******** */

  // 1. Find all positive even numbers less than 10
  val evens = for(i <- 0 until 10 if i%2 == 0) yield i

  // 2. Create an alternating list (size 10) of positive evens and odds less than 10
  //   e.g: List(List(0,2,4,6,8), List(1,3,5,7,9), List(0,2,4,6,8), List(1,3,5,7,9) ... ))
  val alternating = for {
    i <- 0 until 10
    j <- 0 until 10
    if i%2 != j%2
  } yield j

  /* ******** END EXERCISE ******** */
}
