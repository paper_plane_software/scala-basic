import scala.collection.mutable.ListBuffer

object B_Collections {
  // WARNING! WE'RE GOING TO BE USING A LOT OF DIFFERENT SYNTAX RULES NOW. THINGS MAY START TO LOOK STRANGE BUT YOU HAVE
  // ALREADY SEEN THE SYNTAX RULES BEING USED.

  // ** SHOW THE TYPE HIERARCHY SLIDE
  // Lets take a brief moment the look at the basic type hierarchy in Scala

  val immutable = List(1,2,3)
  val mutable = ListBuffer(1,2,3)

  // We will focus only on immutable sequences

  // LISTS
  // Unlike Tuples, Lists are homogeneous linked lists (they only allow a single type)
  // It is possible to have heterogeneous lists (Shapeless), that's beyond the scope of this course
  val xs = List(1,2,3)  // linked-list
  val ys = Array(1,2,3) // array
  // what's the difference?

  // Empty list IS a Nil
  val empty = List()
  val nil = Nil

  // CONS
  // Lists have a special function called cons :: for creating lists
  val zs = 1 :: 2 :: 3 :: Nil // This is actually another syntax trick, any method that is used as an operator operates
                              // on it's LEFT operand (i.e. a.*(b) calls * on a passing b as param), unless the method
                              // ends with a colon in which case it operates on the RIGHT operand.

  // Cons is actually pretty complicated and I never remember everything about how they're implemented. If we have time
  // later we can look into it in more detail. It's actually both a method AND a class (which allows for pattern
  // matching)


  // IMPORTANT FUNCTIONS
  // There are a few very important functions used in the Scala collections library. We'll start with the most commonly
  // used.

  // FOREACH
  // foreach takes a higher-order function/closure and applies the function to each element of the list.
  xs foreach println  // WHY ARE WE ABLE TO WRITE THIS SYNTAX? WHAT'S HAPPENING HERE?

  // MAP
  // Map works just like foreach except that it creates a new list from the result of the passed function applied to
  // each element of the list

  // ** SHOW SLIDE FOR MAP FUNCTION

  val xsDoubled = xs map (x => x*2)

  // ** GOTO EXERCISES 2.3

  // Notice how the size of the list doesn't change? This is important! Map does NOT change list size.

  // FLATTEN
  // Flatten is a very useful function which converts a List[List[T]] to a List[T]. It only flattens one level though,
  // e.g. List[List[List[T]]] => List[List[T]]

  // ** SHOW SLIDE FOR FLATTEN FUNCTION

  val xxs = List(List(1,2), List(3,4)) // What happens to empty lists?
  xxs.flatten

  // What happens to empty lists?
  val yyz = List(List(1,2), Nil, List(3,4)) // This will be important later when we look at monadic types (Options etc)

  // FLATMAP
  // flatmap combines map and flatten.

  // ** SHOW SLIDE FOR FLATMAP

  (0 until 10) flatMap { x =>
    List(s"$x.1", s"$x.2", s"$x.3")
  }
  // what does this code do?

  // ** GOTO EXERCISES 2.4

  // FILTER
  // Does exactly what it says on the tin. Filters out anything that doesn't meet a predicate.

  xs filter (_ < 10) // write this code out in long hand (i.e. without all the syntax sugar)
  xs.filter(x => x < 10)

  // BIG REVEAL
  // Ok, lets pause here for a second, we've learnt some useful functions in the collections library
  // (foreach, map, flatMap and filter). Lets take each of these and take a brief look back at something we learnt about
  // earlier; the For Comprehension

  (0 until 10).foreach(x => println(x)) // Rewrite this line using a for-comprehension
  for(x <- 0 until 10) println(x)       // Ans.

  (0 until 10).filter(_ < 5).map(_ => true) // Rewrite this line using a for-comprehension
  for(x <- 0 until 10 if x < 5) yield true  // Ans

  val tripled1 = (0 until 10).map(x => x*3)     // Rewrite this line using a for-comprehension
  val tripled2 = for(x <- 0 until 10) yield x*3 // Ans.

  val bullets1 = (0 until 10).flatMap(x => List(s"$x.1", s"$x.2"))
  val bullets2 = for {
    x <- 0 until 10
    b <- List(s"$x.1", s"$x.2")
  } yield b
  val WRONG = for(x <- 0 until 10) yield List(s"$x.1", s"$x.2") // Look at the type signature!? Its a List[List[T]]
                                                                // that's wrong!

  // What you've just written aren't similar blocks of code THEY ARE IDENTICAL. For comprehensions are just syntax-sugar
  // they don't really exist

  // GOTO EXERCISE 2.5

  // FOLD / REDUCE

  // ZIP
}
