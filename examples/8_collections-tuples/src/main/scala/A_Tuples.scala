object A_Tuples {
  // TUPLES
  // a tuple is just a loose grouping of values
  // technically a tuple is 2 values, but the term is used for any number of values
  val name1: Tuple2[String, String] = ("Oliver", "Winks")
  val name2: (String, String) = ("Oliver", "Winks")       // (String, String) is shorthand for Tuple2[String, String]

  // the values don't have to be the same type
  val person = ("Oliver Winks", 35) // (String, Int)

  // Like functions you have to draw the line somewhere, again we see the magical number 22

  // UNIT
  // There is no Tuple0 in Scala because it maps to Unit, basically a Tuple0 is nothing, void or Unit
  val unit: Unit = ()

  // ACCESS
  // Accessing tuples values is similar to accessing elements of an Array
  val name = person._1  // The values are 1-based (this is a throw back to older functional languages)

  // CREATING TUPLES
  // All objects implement a special '->' function which takes any value and returns a tuple
  val name3 = "Oliver" -> "Winks"
  val person2 = "Oliver" -> "Winks" -> 35 // what do you think the type of this is?

  // WHY?
  // Tuples are really useful for creating very simple types
  type Person = (String, Int) // This is new, it's called a type alias and it's often used to give names tuples

  // Tuples are useful for returning multiple values from a function
  def mod(dividend: Int, divisor: Int) = {
    val div = dividend / divisor
    val rem = dividend % divisor
    (div, rem)
  }

  /* ******** EXERCISES ******** */

  // 1. Create a function that takes a string and returns the original string along with a "preview" of the string.
  // The first 20 letters for example

  // 2. Create a function that returns the initials of a name

  // 3. Create a PhoneBook class that maps names (String) to phone numbers (Int) implemented using tuples. The class
  // needs an add, remove and contains function, the PhoneBook should not allow duplicate names

  // 4. You've just created a basic HashMap AKA Dictonary, knowing what you do about generic functions, try to make a
  // generic HashMap

  /* ******** END EXERCISES ******** */
}
