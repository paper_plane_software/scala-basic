object Functions {

  // Defining functions
  def funcJavaLike(i: Int): Int = {
    return i * 2
  }
  def funcLastExpr(i: Int): Int = {
    i * 2
  }
  def funcOneLine(i: Int): Int = i*2
  def funcTypeInference(i: Int) = i*2

  // arity 0
  def arity0: Int = 42
  def arity1(x: Int): Int = x*2
  def arity2(x: Int, y: Int): Int = x*y

  def defaultValue(i: Int = 42) = i*2
  def varArgs(xs: Int*) = xs.sum

  def >|^|<(i: Int) = i*2  // function names can be symbolic !*!WARNING!*! USE CAREFULLY

  // Applying functions
  // zero arity and arity one functions have special syntax!
  arity0      // no need to provide empty parenthesis if arity0 - looks like a variable right? Why is that?
              // Ans. In a functional language where functions are pure an unparameterised functions will always return
              // the same thing just like an immutable variable. How does this relate to lazy variables?
  arity1(42)
  // or
  arity1 { 42 } // spread over multiple lines this looks like part of the language right? You'll see why this is useful
                // in a bit
  arity2(42, 43)
  defaultValue()  // We don't need to provide an argument because a default one is defined.
                  // We can't get rid of the parenthesis though because this function is NOT zero arity
  varArgs(42, 43, 44)
  varArgs(List(1,2,3) :_*)  // We haven't go onto lists yet but it's possible to convert lists into var agrs albeit with
                            // strange syntax!
  >|^|<(42) // WTF?!?
}
