object B_StipplePattern {
  // 1. Find all positive even numbers less than 10
  val evens = for(i <- 0 until 10 if i%2 == 0) yield i

  // 2. Create an alternating list (size 10) of positive evens and odds less than 10
  //   e.g: List(List(0,2,4,6,8), List(1,3,5,7,9), List(0,2,4,6,8), List(1,3,5,7,9) ... ))
  val alternating = for {
    i <- 0 until 10
    j <- 0 until 10
    if i%2 != j%2
  } yield j
}
