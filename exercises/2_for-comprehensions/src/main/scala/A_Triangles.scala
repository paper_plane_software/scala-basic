object A_Triangles {

  // 1. Representing Triangles as List[Int], create a list of all triangles with sides less than 10
  val exampleTriangle1 = List(1, 5, 6)  // hyp = 1, adj = 5, opp = 6
  val exampleTriangle2 = List(9, 4, 2)  // hyp = 9, adj = 4, opp = 2

  val sides = 1 to 10 // doesn't make sense to have a side length of 0
  val tris = for {
    hyp <- sides
    adj <- sides
    opp <- sides
  } yield List(hyp, adj, opp)


  // 2. Find all the right-angle triangles in that list
  val rightAngles = for {
    hyp <- sides
    adj <- sides
    opp <- sides
    if hyp*hyp == adj*adj + opp*opp   // could import math.pow
  } yield List(hyp, adj, opp)

  // 3. Find all triangles with sides totalling 24
  val twentyFour = for {
    hyp <- sides
    adj <- sides
    opp <- sides
    if hyp*hyp == adj*adj + opp*opp
    if hyp + adj + opp == 24
  } yield List(hyp, adj, opp)
}
