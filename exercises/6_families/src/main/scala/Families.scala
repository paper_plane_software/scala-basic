object Families {
  sealed trait Person {
    def pronoun: String
    def name: String
    def age: Int
  }
  case class Male(name: String, age: Int, pronoun: String = "Mr") extends Person
  case class Female(name: String, age: Int, pronoun: String = "Ms") extends Person
  case class GenderNeutral(name: String, age: Int, pronoun: String) extends Person

  case class Family(parents: List[Person], children: List[Person])

  val oli = Male("Oliver Winks", 35)
  val sarah = Female("Sarah Bond", 34)
  val astrid = Female("Astrid Winks", 0)

  val myFamily = Family(List(oli, sarah), List(astrid))

  myFamily match {
    case Family(Male(name, _, _) :: Nil, _) => println(name)   // single father
    case Family((p: Person) :: Nil, _) => println(p.name)      // single parent
    case Family(Nil, orphans) => orphans foreach (child => println(child.name)) // orphans
    case Family(parents, _) =>  parents.filter(_ match {
                                  case p: GenderNeutral => true
                                  case _ => false
                                }).foreach(parent => println(parent.pronoun))
  }
}
