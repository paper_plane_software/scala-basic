object HigherOrderFunctions {
  // 1. Write a function (give it an appropriate name) that takes a function (you decide the signature of the function)
  // and calls it ten times
  def repeat10Times(func: () => Unit): Unit = for(_ <- 0 until 10) func()

  // 2. Generalise the function so that it repeats `n` times
  def repeatN(n: Int, func: () => Unit): Unit = for(_ <- 0 until n) func()

  // 3. Write a function (give it an appropriate name) that generates a seq of ints by mapping 0 through `n` to ints
  def listFactory(n: Int, func: (Int) => Int): Seq[Int] = for(i <- 0 to n) yield func(i)

  // 4. Change the function signature so that we can call it with functions defined in the math package (sqrt, cos, sin
  // etc)
  def listFactoryDouble(n: Int, func: (Double) => Double): Seq[Double] = for(i <- 0 to n) yield func(i.toDouble)
  listFactoryDouble(100, math.sqrt)
}
