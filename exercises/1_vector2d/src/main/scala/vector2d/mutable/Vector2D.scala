package vector2d.mutable

object Vector2D {
  def apply(x: Double = 0, y: Double = 0): Vector2D = new Vector2D(x, y)
  def apply(that: Vector2D): Vector2D = new Vector2D(that)
  def origin: Vector2D = Vector2D()
}
class Vector2D(var x: Double, var y: Double) {
  def this(that: Vector2D) {
    this(that.x, that.y)
  }

  def +=(scalar: Double): Unit = {
    x += scalar
    y += scalar
  }
  def -=(scalar: Double): Unit = this += -scalar
  def *=(scalar: Double): Unit = {
    x *= scalar
    y *= scalar
  }
  def /=(scalar: Double): Unit = this *= (1/scalar)

  def +=(that: Vector2D): Unit = {
    x += that.x
    y += that.y
  }
  def -=(that: Vector2D): Unit = this += Vector2D(-that.x, -that.y)

  def *=(other: Vector2D): Unit = {
    x *= other.x
    y *= other.y
  }
  def /=(that: Vector2D): Unit = this *= Vector2D(1/that.x, 1/that.y)

  // Note: What are some problems with this function?
  def update(i: Int, value: Double): Unit = {
    if(i == 0) {
      this += Vector2D(value)
    } else {
      this += Vector2D(0, value)
    }
  }

  def ==(other: Vector2D): Boolean = x == other.x && y == other.y
  def !=(other: Vector2D): Boolean = !(this == other)

  def negate(): Unit = Vector2D(-x, -y)
  def magnitude(): Double = math.sqrt(x*x + y*y)
  def normalise(): Unit = this /= magnitude

  def distance(other: Vector2D): Double = math.sqrt(math.pow(x - other.x, 2) + math.pow(y - other.y, 2))
  def rotate(theta: Double): Unit = {
    this.x = math.cos(theta)
    this.y = math.sin(theta)
  }
}
