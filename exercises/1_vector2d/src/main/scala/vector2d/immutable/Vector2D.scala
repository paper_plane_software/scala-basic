package vector2d.immutable

object Vector2D {
  def apply(x: Double = 0, y: Double = 0): Vector2D = new Vector2D(x, y)
  def apply(that: Vector2D): Vector2D = Vector2D(that.x, that.y)

  // NOTE: just to show how to use apply function
  def origin: Vector2D = Vector2D()
}
class Vector2D(val x: Double, val y: Double) {
  def this(that: Vector2D) {
    this(that.x, that.y)
  }

  def +(scalar: Double): Vector2D = Vector2D(x + scalar, y + scalar)
  def -(scalar: Double): Vector2D = this + -scalar
  def *(scalar: Double): Vector2D = Vector2D(x*scalar, y*scalar)
  def /(scalar: Double): Vector2D = this * (1/scalar)

  def +(that: Vector2D): Vector2D = Vector2D(x + that.x, y + that.y)
  def -(that: Vector2D): Vector2D = this + -that
  def *(that: Vector2D): Vector2D = Vector2D(x*that.x, y*that.y)
  def /(that: Vector2D): Vector2D = this * (that / 1)

  def ==(that: Vector2D): Boolean = x == that.x && y == that.y
  def !=(that: Vector2D): Boolean = !(this == that)

  def unary_-(): Vector2D = Vector2D(-x, -y)

  def magnitude(): Double = math.sqrt(x*x + y*y)
  def normal(): Vector2D = this / magnitude

  def distance(that: Vector2D): Double = (this - that).magnitude
  def rotate(theta: Double): Vector2D = Vector2D(math.cos(theta), math.sin(theta))
}
