object Triangle {
  def apply(p0: Vector3D, p1: Vector3D, p2: Vector3D): Triangle = new Triangle(p0, p1, p2)
}
class Triangle(val p0: Vector3D, val p1: Vector3D, val p2: Vector3D) {
  lazy val centroid: Vector3D = (p0 + p1 + p2) / 3.0
}
