object Main {
  val mesh = for(i <- 0 until 10) yield Triangle(Vector3D.random(), Vector3D.random(), Vector3D.random())

  def translate(dv: Vector3D, tri: Triangle): Triangle = Triangle(tri.p0 + dv, tri.p1 + dv, tri.p2 + dv)
  def extrude(z: Double, tri: Triangle): List[Triangle] = List(tri, translate(Vector3D(z = z), tri))
  def subdivide(tri: Triangle): List[Triangle] = {
    val centre = tri.centroid
    List(Triangle(centre, tri.p0, tri.p1), Triangle(centre, tri.p1, tri.p2), Triangle(centre, tri.p2, tri.p0))
  }

  val newMesh = for {
    tri <-  mesh
    sub <-  subdivide(tri)                  // flatmap
    ext <-  extrude(1.5, sub)               // flatmap
  } yield translate(Vector3D(1, 2, 3), ext) // map

  val aMesh = mesh.flatMap(tri => {
    subdivide(tri).flatMap(sub => {
      extrude(1.5, sub).map(ext => {
        translate(Vector3D(1,2,3), ext)
      })
    })
  })
}
