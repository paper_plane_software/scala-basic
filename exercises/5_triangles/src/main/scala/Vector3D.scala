object Vector3D {
  def apply(x: Double = 0, y: Double = 0, z: Double = 0): Vector3D = new Vector3D(x, y, z)
  def apply(that: Vector3D): Vector3D = new Vector3D(that)
  def random(): Vector3D = new Vector3D(math.random(), math.random(), math.random())
}
class Vector3D(val x: Double, val y: Double, val z: Double) {
  def this(that: Vector3D) {
    this(that.x, that.y, that.z)
  }

  def +(scalar: Double): Vector3D = Vector3D(x + scalar, y + scalar, z + scalar)
  def -(scalar: Double): Vector3D = this + -scalar
  def *(scalar: Double): Vector3D = Vector3D(x*scalar, y*scalar, z*scalar)
  def /(scalar: Double): Vector3D = this * (1/scalar)

  def +(that: Vector3D): Vector3D = Vector3D(x + that.x, y + that.y, z*that.z)
  def -(that: Vector3D): Vector3D = this + -that
  def *(that: Vector3D): Vector3D = Vector3D(x*that.x, y*that.y, z*that.z)
  def /(that: Vector3D): Vector3D = this * (that / 1)

  def ==(that: Vector3D): Boolean = x == that.x && y == that.y && z == that.z
  def !=(that: Vector3D): Boolean = !(this == that)

  def unary_-(): Vector3D = Vector3D(-x, -y, -z)

  def magnitude(): Double = math.sqrt(x*x + y*y + z*z)
  def normal(): Vector3D = this / magnitude

  def distance(that: Vector3D): Double = (this - that).magnitude
  def rotate(theta: Double): Vector3D = Vector3D(math.cos(theta), math.sin(theta))
}
