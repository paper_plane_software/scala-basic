package polygon2d

import vector2d.immutable.Vector2D

class Polygon2D(val vectors: Seq[Vector2D]) extends Transformable {
  override def move(dv: Vector2D): Transformable = new Polygon2D(for(v <- vectors) yield v + dv)
  override def rotate(theta: Double): Transformable = new Polygon2D(for(v <- vectors) yield v rotate theta)
}
