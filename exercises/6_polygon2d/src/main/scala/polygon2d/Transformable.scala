package polygon2d

import vector2d.immutable.Vector2D

trait Transformable {
  def move(dv: Vector2D): Transformable
  def rotate(theta: Double): Transformable
}
