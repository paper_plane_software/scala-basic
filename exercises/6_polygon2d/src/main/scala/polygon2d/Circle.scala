package polygon2d

import vector2d.immutable.Vector2D

class Circle(val centre: Vector2D, val radius: Double) extends Transformable {
  def intersect(other: Circle): Boolean = (centre distance other.centre) < radius
  def intersect(v: Vector2D): Boolean = (v distance centre) < radius

  override def move(dv: Vector2D): Transformable = new Circle(centre + dv, radius)
  override def rotate(theta: Double): Transformable = new Circle(centre rotate theta, radius)
}
