import Dependencies._

lazy val commonSettings = Seq(
  organization := "software.paperplane",
  scalaVersion := "2.12.7",
  version := "1.0-SNAPSHOT"
)

lazy val root = (project in file("."))
  .aggregate(
    // day 1
    helloWorld,
    helloJava,
    variables,
    functionsI,
    classesI,
    vector2d,

    // day 2
    branchingLooping,
    functionsII,
    collectionsTuples,
    forComprehensions,
    higherOrderFunctions,
    tuplesMap,
    triangles,

    // day 3
    classesII,
    patternMatching,
    classesIII,
    families,
    polygon2d
  )

/** *************************************************************
  * Day 1
  * *************************************************************/
// live coding
lazy val helloWorld = (project in file("./examples/1_hello-world"))
  .settings(
    inThisBuild(commonSettings),
    name := "hello-world"
  )

lazy val helloJava = (project in file("./examples/2_hello-java"))
  .settings(
    inThisBuild(commonSettings),
    name := "hello-java"
  )

lazy val variables = (project in file("./examples/3_variables"))
  .settings(
    inThisBuild(commonSettings),
    name := "variables"
  )

lazy val functionsI = (project in file("./examples/4_functions-i"))
  .settings(
    inThisBuild(commonSettings),
    name := "functions-i"
  )

lazy val classesI = (project in file("./examples/5_classes-i"))
  .settings(
    inThisBuild(commonSettings),
    name := "classes-i"
  )

// exercises
lazy val vector2d = (project in file("./exercises/1_vector2d"))
  .settings(
    inThisBuild(commonSettings),
    name := "vector2d",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )

/** **************************************************************************
  * Day 2
  * **************************************************************************/
// live coding
lazy val branchingLooping = (project in file("./examples/6_branching-looping"))
  .settings(
    inThisBuild(commonSettings),
    name := "branching-looping"
  )

lazy val functionsII = (project in file("./examples/7_functions-ii"))
  .settings(
    inThisBuild(commonSettings),
    name := "functions-ii"
  )

lazy val collectionsTuples = (project in file("./examples/8_collections-tuples"))
  .settings(
    inThisBuild(commonSettings),
    name := "collections-tuples"
  )

// exercises
lazy val forComprehensions = (project in file("./exercises/2_for-comprehensions"))
  .settings(
    inThisBuild(commonSettings),
    name := "for-comprehensions",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )

lazy val higherOrderFunctions = (project in file("./exercises/3_higher-order-functions"))
  .settings(
    inThisBuild(commonSettings),
    name := "higher-order-functions",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )

lazy val tuplesMap = (project in file("./exercises/4_tuples-map"))
  .settings(
    inThisBuild(commonSettings),
    name := "tuples-map",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )

lazy val triangles = (project in file("./exercises/5_triangles"))
  .settings(
    inThisBuild(commonSettings),
    name := "triangles",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )


/** *************************************************************
  * Day 3
  * *************************************************************/
// live coding
lazy val classesII = (project in file("./examples/9_classes-ii"))
  .settings(
    inThisBuild(commonSettings),
    name := "classes-ii"
  )

lazy val patternMatching = (project in file("./examples/10_pattern-matching"))
  .settings(
    inThisBuild(commonSettings),
    name := "pattern-matching"
  )

lazy val classesIII = (project in file("./examples/11_classes-iii"))
  .settings(
    inThisBuild(commonSettings),
    name := "classes-iii"
  )

// exercises
lazy val families = (project in file("./exercises/6_families"))
  .settings(
    inThisBuild(commonSettings),
    name := "families",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )
lazy val polygon2d = (project in file("./exercises/6_polygon2d"))
  .dependsOn(vector2d)
  .settings(
    inThisBuild(commonSettings),
    name := "polygon2d",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )
